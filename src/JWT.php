<?php
    /**
     * Created by PhpStorm.
     * User: duchao
     * Date: 2018/4/28
     * Time: 11:07
     */
    
    namespace jwt;

    class JWT
    {
        public static $error;
        public static $config=[
            'iss'=>'store.hnlianzhu.com',
            'aud'=>'ser.hnlianzhu.com',
            'app_id'=>'Lx_82dfee2586d6f3c47318f64ca05595d5',
            'secret'=>'LS_ab6530ec1d2280cbd65c7214900d0a3d',
            'leeway'=>'60'
        ];
        public static function encode($data){
            $config=self::$config;
            $cdata=[
                "iss" => $config['iss'],
                "aud" => $config['aud'],
                'exp'=>time()+$config['exp'],
                "iat" => time(),
                "nbf" => time(),
                "app_id"=>$config['app_id'],
                "secret"=>$config['secret']
            ];
            $key=md5($config['app_id'].$config['secret']);
            $data=array_merge($cdata,$data);
            ksort($data);
        
            return JWTSdk::encode($data,$key);
        }
    
        public static function decode($Authorization){
            try{
                $config=self::$config;
                $key=md5($config['app_id'].$config['secret']);
                JWTSdk::$leeway=$config['leeway'];
                JWTSdk::$timestamp=time();
                $decoded=JWTSdk::decode($Authorization,$key,['HS256']);
                return (array)$decoded;
                
            }catch (\Exception $e){
                self::$error=$e->getMessage();
                return false;
            }
        
        }
    
        public static function getError(){
            return self::$error;
        }
    }